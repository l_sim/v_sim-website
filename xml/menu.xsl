<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">

  <xsl:key name="pageId" match="a" use="@id" />
  <xsl:key name="pageEntry" match="entry" use="@key" />

  <xsl:template name="makeMenu">
    <xsl:param name="lang" />
    <xsl:param name="key" />
    <div class="menu">
      <div class="menuHeader">
        <h2>
          <xsl:for-each select="document('webPages.xml')/webPages/menus/menu/value">
            <xsl:if test="$lang = @lang">
              <xsl:value-of select="text()" />
            </xsl:if>
          </xsl:for-each>
        </h2>
      </div>
      <div class="menuMain">
        <xsl:for-each select="document('menu.xml')/menu/part">
          <!-- Recherche le titre dans la bonne langue -->
          <xsl:variable name="titre">
            <xsl:for-each select="title/value">
              <xsl:if test="@lang = $lang"><xsl:value-of select="text()" /></xsl:if>
            </xsl:for-each>
          </xsl:variable>
          <xsl:if test="$titre = ''"><h3>Titre sans traduction!</h3></xsl:if>
          <xsl:if test="not($titre = '')"><h3><xsl:value-of select="$titre" /></h3></xsl:if>
          <!-- Affiche chaque entrée -->
          <xsl:if test="a">
            <ul>
              <xsl:for-each select="a">
                <li>
                  <xsl:if test="position() = last()">
                    <xsl:attribute name="class">last</xsl:attribute>
                  </xsl:if>
                  <xsl:apply-templates select="." mode="makeMenuEntry">
                    <xsl:with-param name="lang" select="$lang" />
                    <xsl:with-param name="key" select="$key" />
                  </xsl:apply-templates>
                </li>
              </xsl:for-each>
            </ul>
          </xsl:if>
        </xsl:for-each>
      </div>
      <div class="menuFooter"><h2>--+ +--</h2></div>
    </div>
  </xsl:template>

  <xsl:template name="makeSubMenu">
    <xsl:param name="lang" />
    <xsl:param name="parent" />
    <xsl:param name="key" />
    <div class="submenu">
      <xsl:for-each select="document('webPages.xml')/webPages/menus/submenu/value">
        <xsl:if test="$lang = @lang">
          <h3><xsl:value-of select="text()" /></h3>
        </xsl:if>
      </xsl:for-each>
      <xsl:for-each select="document('menu.xml')/menu/part/a">
        <xsl:if test="@id = $parent">
          <xsl:for-each select="subPart/a">
            <xsl:apply-templates select="." mode="makeMenuEntry">
              <xsl:with-param name="lang" select="$lang" />
              <xsl:with-param name="key" select="$key" />
            </xsl:apply-templates>
            <xsl:if test="position() != last()">
              <xsl:text> | </xsl:text>
            </xsl:if>
          </xsl:for-each>
        </xsl:if>
      </xsl:for-each>
    </div>
  </xsl:template>

  <xsl:template match="a" mode="makeMenuEntry">
    <xsl:param name="lang" />
    <xsl:param name="key" />
    
    <xsl:if test="@id = $key">
      <em>
        <xsl:for-each select="document('webPages.xml')/webPages/liens/entry">
          <xsl:if test="$key = @key">
            <xsl:for-each select="value">
              <xsl:if test="@lang = $lang">
                <xsl:value-of select="text()" />
              </xsl:if>
            </xsl:for-each>
          </xsl:if>
        </xsl:for-each>
      </em>
    </xsl:if>
    <xsl:if test="not(@id = $key)">
      <xsl:variable name="prefix"><xsl:if test="$key = 'api'">../</xsl:if></xsl:variable>
      <xsl:call-template name="lien">
        <xsl:with-param name="lang" select="$lang" />
        <xsl:with-param name="langDest" select="@lang" />
        <xsl:with-param name="href" select="@href" />
        <xsl:with-param name="id" select="@id" />
        <xsl:with-param name="texte" select="normalize-space(text())" />
        <xsl:with-param name="prefix" select="$prefix" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="langue">
    <xsl:param name="lang" />
    <xsl:param name="langDest" />
    <xsl:param name="restricted" />
    <xsl:if test="$restricted = 'yes'">
      <span class="warn">
        <xsl:for-each select="document('webPages.xml')/webPages/menus/restricted/value">
          <xsl:if test="@lang = $lang">
            <xsl:value-of select="text()" />
          </xsl:if>
        </xsl:for-each>
      </span>
    </xsl:if>
    <xsl:if test="not($lang = $langDest) and $restricted = 'yes'">
      <xsl:text> | </xsl:text>
    </xsl:if>
    <xsl:if test="not($lang = $langDest)">
      <xsl:for-each select="document('webPages.xml')/webPages/langues/value">
        <xsl:if test="@lang = $lang and @orig = $langDest">
          <xsl:value-of select="text()" />
        </xsl:if>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>

  <xsl:template name="lien">
    <xsl:param name="lang" /><!-- langue courante -->
    <xsl:param name="langDest" /><!-- langue de la page pointée par le lien -->
    <xsl:param name="href" /><!-- adresse de la page (optionnelle) -->
    <xsl:param name="id" /><!-- id du menu de la page (optionnelle) -->
    <xsl:param name="texte" /><!-- texte à mettre dans la balise a -->
    <xsl:param name="param" /><!-- partie à coller après l'url -->
    <xsl:param name="prefix" /><!-- partie à coller avant l'url -->

    <!-- Teste la validité des paramètres -->
    <xsl:if test="not($lang) or $lang = ''">
      <xsl:message>Attention, appel de résolution d'un lien sans le paramètre lang.</xsl:message>
    </xsl:if>
    <xsl:if test="(not($href) or $href = '') and (not($id) or $id = '')">
      <xsl:message>Attention, appel de résolution d'un lien sans un paramètre href ou id.</xsl:message>
    </xsl:if>
    <xsl:if test="$href and not($href = '') and (not($texte) or $texte = '')">
      <xsl:message>Attention, appel de résolution d'un lien avec le paramètre href mais sans texte.</xsl:message>
    </xsl:if>

    <xsl:if test="$id">
      <xsl:for-each select="document('webPages.xml')/webPages/liens/entry">
        <xsl:if test="@key = $id">
          <xsl:for-each select="value">
            <xsl:if test="(@lang = $lang and ($langDest = '' or not($langDest))) or @lang = $langDest">
              <!-- Ok, on a le bon lien dans la bonne langue -->
              <a>
                <xsl:attribute name="href">
                  <xsl:if test="@href">
                    <xsl:if test="$prefix and not($prefix = '')"><xsl:value-of select="$prefix" /></xsl:if><xsl:value-of select="@href" /><xsl:if test="$param and not($param = '')"><xsl:value-of select="$param" /></xsl:if>
                  </xsl:if>
                  <xsl:if test="not(@href)">
                    <xsl:if test="$prefix and not($prefix = '')"><xsl:value-of select="$prefix" /></xsl:if><xsl:value-of select="../default/@href" /><xsl:if test="$param and not($param = '')"><xsl:value-of select="$param" /></xsl:if>
                  </xsl:if>
                </xsl:attribute>
                <xsl:if test="$texte">
                  <xsl:value-of select="$texte" />
                </xsl:if>
                <xsl:if test="not($texte)">
                  <xsl:value-of select="text()" />
                </xsl:if>
              </a>
              <xsl:if test="not(@href) or ../@restricted = 'yes'">
                <span class="langInfo"> [<xsl:call-template name="langue"><xsl:with-param name="lang" select="$lang" /><xsl:with-param name="langDest" select="../default/@lang" /><xsl:with-param name="restricted" select="../@restricted" /></xsl:call-template>]</span>
              </xsl:if>
            </xsl:if>
          </xsl:for-each>
        </xsl:if>
      </xsl:for-each>
    </xsl:if>
    <xsl:if test="$href">
      <a>
        <xsl:attribute name="href">
          <xsl:value-of select="$href" /><xsl:if test="$param and not($param = '')"><xsl:value-of select="$param" />'</xsl:if>
        </xsl:attribute>
        <xsl:value-of select="$texte" />
      </a>
      <xsl:if test="($langDest and not($lang = $langDest)) or @restricted = 'yes'">
        <span class="langInfo"> [<xsl:call-template name="langue"><xsl:with-param name="lang" select="$lang" /><xsl:with-param name="langDest" select="$langDest" /></xsl:call-template>]</span>
      </xsl:if>
    </xsl:if>
  </xsl:template>

  <xsl:template match="liens/entry">
    <xsl:param name="rel" />
    <xsl:param name="type" />
    <xsl:param name="lang" />
    <xsl:if test="@internal = 'yes'">
      <link>
        <xsl:attribute name="rel"><xsl:value-of select="$rel" /></xsl:attribute>
        <xsl:attribute name="type"><xsl:value-of select="$type" /></xsl:attribute>
        <xsl:for-each select="value">
          <xsl:if test="@lang = $lang">
            <!-- l'url est donnée fastoche -->
            <xsl:if test="@href">
              <xsl:attribute name="href"><xsl:value-of select="@href" /></xsl:attribute>
              <xsl:attribute name="title"><xsl:value-of select="text()" /></xsl:attribute>
              <xsl:attribute name="lang"><xsl:value-of select="@lang" /></xsl:attribute>
              <xsl:attribute name="hreflang"><xsl:value-of select="@lang" /></xsl:attribute>
            </xsl:if>
            <!-- l'url n'est pas donnée on récupère défault -->
            <xsl:if test="not(@href)">
              <!-- défault existe cool -->
              <xsl:if test="../default/@href">
                <xsl:attribute name="href"><xsl:value-of select="../default/@href" /></xsl:attribute>
                
                <xsl:attribute name="title"><xsl:value-of select="text()" /><!--<xsl:if test="not(../default/@lang = $lang)"> (<xsl:call-template name="langue"><xsl:with-param name="lang" select="$lang" /><xsl:with-param name="langDest" select="../default/@lang" /></xsl:call-template>)</xsl:if>--></xsl:attribute>
                <xsl:attribute name="lang"><xsl:value-of select="../default/@lang" /></xsl:attribute>
                <xsl:attribute name="hreflang"><xsl:value-of select="../default/@lang" /></xsl:attribute>
              </xsl:if>
              <!-- défault n'existe pas : le lien est cassé -->
              <xsl:if test="not(../default/@href)">
                <xsl:attribute name="href"></xsl:attribute>
                <xsl:attribute name="title"><xsl:value-of select="text()" /> (Broken link)</xsl:attribute>
              </xsl:if>
            </xsl:if>
          </xsl:if>
        </xsl:for-each>
      </link>
    </xsl:if>
  </xsl:template>

  <xsl:template name="makeLinkEntry">
    <xsl:param name="rel" />
    <xsl:param name="type" />
    <xsl:param name="lang" />
    <xsl:param name="key" />

    <xsl:for-each select="document('webPages.xml')/webPages/liens/entry">
      <xsl:apply-templates select="key('pageEntry', $key)">
        <xsl:with-param name="rel" select="$rel" />
        <xsl:with-param name="type" select="$type" />
        <xsl:with-param name="lang" select="$lang" />
      </xsl:apply-templates>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="makeLink">
    <xsl:param name="lang" />
    <xsl:param name="key" />

    <xsl:for-each select="document('webPages.xml')/webPages/liens/entry">
      <xsl:if test="@key = $key">
        <!-- link for alternate languages -->
        <xsl:for-each select="value">
          <xsl:if test="not(@lang = $lang)">
            <xsl:if test="@href">
              <link rel="alternate" type="text/html">
                <xsl:attribute name="href"><xsl:value-of select="@href" /></xsl:attribute>
                <xsl:attribute name="title"><xsl:value-of select="text()" /></xsl:attribute>
                <xsl:attribute name="lang"><xsl:value-of select="@lang" /></xsl:attribute>
                <xsl:attribute name="hreflang"><xsl:value-of select="@lang" /></xsl:attribute>
              </link>
            </xsl:if>
          </xsl:if>
        </xsl:for-each>
        <!-- link prev & next -->
        <xsl:variable name="pos">
          <xsl:value-of select="position()" />
        </xsl:variable>
        <xsl:if test="../*[$pos - 1]">
          <xsl:apply-templates select="../*[$pos - 1]">
            <xsl:with-param name="rel" select="'prev'" />
            <xsl:with-param name="type" select="'text/html'" />
            <xsl:with-param name="lang" select="$lang" />
          </xsl:apply-templates>
        </xsl:if>
        <xsl:if test="../*[$pos + 1]">
          <xsl:apply-templates select="../*[$pos + 1]">
            <xsl:with-param name="rel" select="'next'" />
            <xsl:with-param name="type" select="'text/html'" />
            <xsl:with-param name="lang" select="$lang" />
          </xsl:apply-templates>
        </xsl:if>
      </xsl:if>
      <!-- link start -->
      <xsl:if test="@key = 'index'">
        <xsl:apply-templates select=".">
          <xsl:with-param name="rel" select="'start'" />
          <xsl:with-param name="type" select="'text/html'" />
          <xsl:with-param name="lang" select="$lang" />
        </xsl:apply-templates>
      </xsl:if>
    </xsl:for-each>

    <!-- link section -->
    <xsl:for-each select="document('menu.xml')/menu/part/a">
      <xsl:variable name="id">
        <xsl:value-of select="@id"/>
      </xsl:variable>
      <xsl:for-each select="document('webPages.xml')/webPages">
        <xsl:apply-templates select="key('pageEntry', $id)">
          <xsl:with-param name="rel" select="'section'" />
          <xsl:with-param name="type" select="'text/html'" />
          <xsl:with-param name="lang" select="$lang" />
        </xsl:apply-templates>
      </xsl:for-each>
      <xsl:if test="$key = $id">
        <xsl:for-each select="subPart/a">
          <xsl:variable name="subid">
            <xsl:value-of select="@id"/>
          </xsl:variable>
          <xsl:for-each select="document('webPages.xml')/webPages">
            <xsl:apply-templates select="key('pageEntry', $subid)">
              <xsl:with-param name="rel" select="'subsection'" />
              <xsl:with-param name="type" select="'text/html'" />
              <xsl:with-param name="lang" select="$lang" />
            </xsl:apply-templates>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
