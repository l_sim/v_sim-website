<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" indent="yes" encoding="utf-8"/>

  <xsl:include href="pagesWeb.xsl"/>

  <xsl:template match="/ListeDesChosesÀFaire">
    <libre><xsl:text disable-output-escaping="yes"><![CDATA[<]]></xsl:text>![CDATA[
    <xsl:text disable-output-escaping="yes"><![CDATA[<div class="newsInfos">Dernières modifications le : ]]></xsl:text>
    ]]<xsl:text disable-output-escaping="yes"><![CDATA[>]]></xsl:text>
    </libre>  
    <date>
      <xsl:attribute name="jour">
        <xsl:value-of select="document('date.xml')/date/@jour" />
      </xsl:attribute>
      <xsl:attribute name="mois">
        <xsl:value-of select="document('date.xml')/date/@mois" />
      </xsl:attribute>
      <xsl:attribute name="annee">
        <xsl:value-of select="document('date.xml')/date/@annee" />
      </xsl:attribute>
    </date>
    <libre><xsl:text disable-output-escaping="yes"><![CDATA[<]]></xsl:text>![CDATA[
    <xsl:text disable-output-escaping="yes"><![CDATA[</div>]]></xsl:text>
    <xsl:for-each select="milestone">
      <p>Liste des modifications et ajouts, envisagés ou réalisés concernant la version <xsl:value-of select="@version" /> de V_Sim.</p>
      <xsl:for-each select="objectif">
        <h2><xsl:value-of select="@titre" /></h2>
        <xsl:if test="not(normalize-space(text()) = '')">
          <p><xsl:value-of select="text()" /></p>
        </xsl:if>
        <table border="0" cellpadding="0" cellspacing="0" class="todo">
          <xsl:apply-templates select="étape"/>
        </table>
      </xsl:for-each>
      <hr />
    </xsl:for-each>
    ]]<xsl:text disable-output-escaping="yes"><![CDATA[>]]></xsl:text>
    </libre>  
  </xsl:template>

  <xsl:template match="étape">
    <tr>
      <td>
        <xsl:attribute name="class">
          <xsl:choose>
            <xsl:when test="@état = 'OK'">
              <xsl:text>etatTODO_OK</xsl:text>
            </xsl:when>
            <xsl:when test="@état = 'En cours'">
              <xsl:text>etatTODO_enCours</xsl:text>
            </xsl:when>
            <xsl:when test="@état = 'Projet'">
              <xsl:text>etatTODO_projet</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:message>
                <xsl:text>ATTENTION! état inconnu.</xsl:text>
              </xsl:message>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:text>::</xsl:text>
      </td>
      <td>
        <xsl:value-of select="text()" />
      </td>
    </tr>
  </xsl:template>

</xsl:stylesheet>

