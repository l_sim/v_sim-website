<?xml version="1.0" encoding="utf-8"?>

<page key="tuto-python" have-menu="yes" lang="en">
  <title>tutorial - Python scripting</title>
  <main>
    <libre><![CDATA[
<h1>Tutorial: Python scripting</h1>
<div class="newsInfos">Author: Damien Caliste</div>
<p>This tutorial requires at least <span class="warn">version 3.7</span>.</p>
<p>This tutorial explains how to use the scripting capabilities of
V_Sim. The Python bindings are provided by <a href="https://live.gnome.org/GObjectIntrospection">GObject-introspection</a> and
makes almost all internal C routines of V_Sim callable through
Python. So scripting capabilities can be used both to use existing
functions through scripts but also to extend capabilities of V_Sim by
implementing new functions.</p>
<p>During this tutorial, one will learn more about the Python
bindings. It is adviced to keep at hand the ]]></libre><a
id="api">reference page</a><libre><![CDATA[ where all public functions
of V_Sim are documented.</p>
<p>All Python scripts should begin with the line loading V_Sim module
from GObject-introspection:</p>
<pre>
from gi.repository import v_sim
</pre>
    ]]></libre>

    <section id="python-panel">The new Python panel</section>
    <libre><![CDATA[
<img style="float:right;" src="images/tutorials/panel_python.png"
alt="New Python panel for scripting" />
<p>The scripting capabilities are to be used within V_Sim. To allow
this, a new panel called 'scripts Python' is available. This panel is
divided into two parts.</p>
<ul>
  <li>The first part lists loaded scripts on startup, as saved in the
  parameter file. This is convenient to automatically load scripts
  extending V_Sim capabilities, for instance to <a
  href="#add_input">add new input formats</a>. One can add or remove
  scripts in this list by using the buttons on the right of the
  list.</li>
  <li>The second part of the panel deals with interactive execution of
  scripts. One pick up a script and execute it with the provided
  buttons. The standard Python output and error outpu are redirected
  to the text buffer below the buttons. One can empty this buffer with
  the button in the toolbar on the right.</li>
</ul>
<p>Interactive scripts can be loaded from command line using the
<b><code>-o&nbsp;pyScript=path/to/python/script.py</code></b> option. The
script is then executed after loading any data file from the command
line.</p>
<p>One can execute as many time as desired the currently loaded
interactive script. This allows for easy debugging of a script or to
change some parameters before executing again.</p>
    ]]></libre>

    <section id="VisuData">Dealing with the main object storing nodes</section>
    <libre><![CDATA[
<h2>Getting already loaded data</h2>
<p>The central object in V_Sim is the object storing the nodes and all
related informations like the bounding box, the boundary
conditions... By convention, there is only one rendered data object at
once and this object is thus an attribute of the rendering window. One
can access this object by calling the current rendering window and
retrieving its data:</p>
<pre>
render = v_sim.UiMainClass.getDefaultRendering()
dataObj = render.getVisuData()
</pre>
<h2>Loading data from disk</h2>
<p>On the opposite, one can ask V_Sim to load a new data from
file. This is done by creating a data object
(<code><a
href="api/v-sim-visu-data.html#visu-data-new">v_sim.Data.new</a></code>),
adding the name of a file to this object and ask the rendering window to load this
file. One need to actually add filenames before loading, because
complex rendering method like spin rendering may require
to add several input files before loading.</p>
<pre>
dataObj = v_sim.Data.new()
dataObj.addFile("posout_0010.ascii", 0, None)
render = v_sim.UiMainClass.getDefaultRendering()
render.loadFile(dataObj, 0)
v_sim.ui_wait()
</pre>
<p>The last call to <code>v_sim.ui_wait</code> make V_Sim process the
loading action before continuing to execute the script. This is
necessary because the rendering window postpone in the event loop the
actual load of a file.</p>

<h2>Associated functions to data objects</h2>
<p>Data objects inherit from the <code><a
href="api/VisuNodeArray.html">v_sim.NodeArray</a></code> class. This class
has different routines to deal with a set of nodes, including signals
to detect changes in population definition (nodes have been added or
removed) or changes in rendering (nodes are hidden or change
position). In V_Sim, nodes are identified by a unique number ranging
from 0. Most of the functions dealing with nodes have this number as
an input. When one has a Node object, its number is retrieved with
<code>v_sim.Node.number</code> while on the contrary, a number can be used
to retrieve a Node by calling
<code>v_sim.NodeArray.getFromId(number)</code>.</p>
<p>One can iterate over nodes by using built-in iterators. There are
several ones:</p>
<ul>
  <li><code>__iter__</code>, the default one, iterates on all nodes,
  in the in-memory order ;</li>
  <li><code>__iterByNumber__</code>, like the default one, iterates on
  all nodes but following the order of the input file ;</li>
  <li><code>__iterElements__</code>, iterate only on the elements of
  the node array ;</li>
  <li><code>__iterVisible__</code>, like the default one, iterates on
  all nodes, but restrict to visible nodes only ;</li>
  <li><code>__iterOriginal__</code>, like the default one, iterate on
  all nodes, but restrict to original nodes only (don't iterate on
  node obtained by duplication of the super-cell) ;</li>
  <li><code>__iterByPairs__</code>, iterate on all set of two nodes.</li>
</ul>
<p>As example, here is a code that displays for all visible nodes of
<code>data</code> their number, element name and coordinates:</p>
<pre>
for it in data.__iterVisible__():
  print "Node %04d ('%s') at " % (it.node.number, it.element.getName()) + \
        str(data.getNodeCoordinates(it.node))
</pre>

    ]]></libre>

    <section id="additional">Additionnal capabilities</section>
    <libre><![CDATA[
<h2>Data colourisation</h2>
<p>There are two methods to create colourisation, from file or from
data ; and there is one method to retrieve current colourisation. Here
is an example of seting up colourisation from a numpy array called
<code>data</code> with 5 columns of data:</p>
<pre>
(cl, new) = v_sim.Colorization.new_fromData(dataObj, 5, data.reshape(-1))
cl.setShade(v_sim.Shade.getList()[4])
cl.setRestrictInRange(True)
cl.setScaleType(v_sim.ColorizationInputScaleId.MINMAX)
cl.setColUsed(1, 0)
cl.setColUsed(1, 1)
cl.setColUsed(1, 2)
cl.setMin(-0.04, 0)
cl.setMax(+0.04, 0)
v_sim.UiPanel.colorization_setUsed(True)
</pre>
<p>In this example, the colourisation is taken for a predefined shade
(the 5<sup>th</sup> one), and the scaling is defined manually within
[-0.04; +0.04].</p>

<p>Another example is to get an already defined colourisation (setup
from command-line for instance) and to change its properties like the
masking property:</p>
<pre>
(cl, new) = v_sim.Colorization.get(dataObj, False)

def logHiding(cl, values, data):
  return (numpy.log10(values.data[0]) &lt; -7)
cl.setHidingFunc(logHiding, None)
</pre>
<p>In this example, the default hiding function is replaced by a
Python one called <code>logHiding</code> which hide every node which
data from first column is below 10<sup>-7</sup>. To activate this new
hiding function, one has to call the following code additionally to
ask V_Sim to indeed to the hiding action:</p>
<pre>
dataObj.askForShowHide()
if cl.applyHide(dataObj):
  dataObj.emit("RenderedChanged")
  dataObj.createAllNodes()
  v_sim.Object.redraw(None)
</pre>
<p>The first function <code>v_sim.NodeArray.askForShowHide</code> will apply hiding
properties on all nodes for other parts of V_Sim (like planes). Then,
we call the hiding routine of colourisation with
<code>v_sim.Colorosization.applyHide()</code> function. Then, we rebuild OpenGL list
representing nodes and redraw.</p>

<h2>Planes</h2>
<p>Plane can be built from scripts also. To show them, one need to add
them in the plane panel. To built a plane, one need to provide a box
(to indeed draw the plane within this box). One usually use the data
object box, as shown on this example where we create a [1, 1, 1] plane
at 5.89 of distance from origin of the box with the first preset colour:</p>
<pre>
render = v_sim.UiMainClass.getDefaultRendering()
dataObj = render.getVisuData()
plane = v_sim.Plane.new(dataObj.getBoxVertices(False), [1,1,1], 5.89,
                        v_sim.color_getById(0))
</pre>
<p>Finally, the plane is added to the plane panel and this one is
activated. In this example, the plane is used for hiding (the first
boolean value) and a direct hiding side is used (the second boolean value):</p>
<pre>
v_sim.UiPanel.planes_setUsed(True)
v_sim.UiPanel.planes_add(plane, True, False)
</pre>

<h2>Scalar fields</h2>
<p>Scalar field are a set of data on a grid, uniform or not. Let's
introduce here simple scalarfield, representing densities on a regular
grid:</p>
<pre>
field = v_sim.ScalarField.new("Density at iter %d" % it)
field.setPeriodic(True)
field.setBox((alat[0], 0., alat[1], 0., 0., alat[2]))
field.setGridSize((n1, n2, n3))
field.setData(density, True)
</pre>
<p>In this example, density is a 1D array containing (n1 × n2 × n3)
values in a zyx order. xyz order is also supported by passing False as
the second argument to the <code>setData</code> routine.</p>

<h2>Iso-surfaces</h2>
<p>The Surfaces object is used in V_Sim to represent
iso-surfaces. This object can indeed store several surfaces. A
Surfaces object is created with:</p>
<pre>
surf = v_sim.Surfaces.new(0)
surf.setBox((alat[0], 0., alat[1], 0., 0., alat[2]))
</pre>
<p>As for the plane, the surfaces will be rendered if added to the
surface panel:</p>
<pre>
v_sim.UiPanel.surfaces_setUsed(True)
iterSurf = v_sim.UiPanel.surfaces_addSurfaces(surf, "Density")
</pre>
<p>Currently, the Surfaces object contains no surface. One need to add
some new. One can use directly the <code>v_sim.Surfaces.allocate()</code> or the
<code>v_sim.Surfaces.add()</code> functions, but usually one uses a creation method
computering a new surface from a density field:</p>
<pre>
(res, surf) = v_sim.Surfaces.createFromScalarField(surf, field, 0.08, it, "Iter %d" % it)
</pre>

<h2>Coloured maps</h2>
<p></p>
    ]]></libre>

    <section id="add_input">Adding a new input file format</section>
    <libre><![CDATA[
<p>One can add input file format by providing a parsing routine. This
parsing routine takes a filename and should return True if the
filename is a valid file for this new input format (even with errors)
or False if the file is not of this format. Here is an
example of generic parsing routine:</p>
<pre>
def parseFoo(data, filename, format, iset, cancel):
  valid = False
  try:
    # Read the given file and decide if it conform to this file format.
    f = open(filename, "r")
    ...
    if not fooFormat:
      return False
    valid = True

    # Pass the variables to V_Sim.
    ...

    return True
  except Exception as error:
    print type(error), error
    return valid
  return False
</pre>
<p>If read data are of a valid format, they should be transfered in the
input V_Sim data object, allocates its storing space for
nodes and setup things like boundary conditions, commentary...:</p>
<pre>
       # Declare one set of data for this file.
    data.setNSubset(1)
       # Setup the box geomtry.
       # Setting up the box geometry here make available to use the
       # converting routines to go from reduced coordinates to cartesian.
    data.setBoxGeometryFull(box, v_sim.DataBoxBoundaries.PERIODIC)
       # Allocate the internal arrays for these elemnts and number of nodes.
    data.allocateByNames(nElements, elements)
    for (coord, ele) in zip(coords, allElements):
         # For each node, add it with its coordinates
      data.addNodeFromElementName(ele, coord, False, False);
       # Mandatory call at the end to update some internal dimensions
       # Related to the box.
    data.applyBoxGeometry()
       # Set a commentry for this subset of data.
    data.setFileCommentary(desc.strip(), 0)
</pre>
<p>Finally, the new parsing routine should be declared to V_Sim as a
parsing routine, providing description strings... (here the new
parsing method is using for atomic visualisation, but routine for spin
should look the same):</p>
<pre>
# Declare this new fileformat to V_Sim.
fmt = v_sim.FileFormat.new("Foo file format", ("*.foo", "*.FOO", "*.foox"))
v_sim.Rendering.getByName("Atom visualisation").addFileFormat(0, fmt, 95, parseFoo)
</pre>
<p>The priority value (here 95) is used to sort the parsing routine,
the lower the priority value, the sooner the routine is used to load a
file. Loose file format should have a higher value (they are tested
later) while strict file format should have a lower value (they can be
tested early without raising false positive).</p>
    ]]></libre>

    <section id="dump">Dumping actions</section>
    <libre><![CDATA[
<p>One can call the dumping routine of V_Sim to generate images or
data files in other formats. The general case use the rendering window
to export:</p>
<pre>
render = v_sim.UiMainClass.getDefaultRendering()
render.dump(v_sim.Dump.png_getStatic(), "output.png", 750, 750, None, None)
</pre>
<p>But, if the format does not require OpenGL, one can use directly
the <code>v_sim.Dump.write()</code> routine of a dump module applied on a data
object:</p>
<pre>
v_sim.Dump.abinit_getStatic().write("ab.in", -1, -1, dataObj, None, None, None)
</pre>
<p>In this case, the width, height and image array argument of the
<code>v_sim.Dump.write()</code> routine are ignored.</p>
    ]]></libre>

    <section id="gui">Adding new interface elements</section>
    <libre><![CDATA[
<h2>Creating a new panel</h2>
<p>Python scripting can be used also to create additional elements in
the user interface. Here is an example for a new panel in the command
window.</p>
<p>V_Sim defines its own object called UiPanel. One can create one
easily by issuing:</p>
<pre>
panel = v_sim.UiPanel.new("mytools", "Customized tools", "My tools")
panel.setDockable(True)
panel.attach(v_sim.UiPanelClass.getCommandPanel())
</pre>
<p>The UiPanel is a Gtk container, so it can be populated easily with
<code>gtk.Container.add()</code> as any other Gtk container.</p>

<h2>Selecting a node by clicking on it</h2>
<p>V_Sim introduces a object called Interactive. This object is used
to interact with the 3D view. The rendering window has a stack of used
interactive object and send action to the last added one. Here is how
to create a new interactive pick session and start it on the rendering
window:</p>
<pre>
interPick = v_sim.Interactive.new(v_sim.InteractiveId.PICK)
render = v_sim.UiMainClass.getDefaultRendering()
render.pushInteractive(interPick)
render.pushMessage("Pick a node with the mouse")
</pre>
<p>To stop our custom pick session, one should call:</p>
<pre>
render.popInteractive(interPick)
render.popMessage()
</pre>
<p>Pushing and poping a message in the status bar is not mandatory but
welcome !</p>
<p>One can connect an action on the 'node-selection' signal emitted by
the Interactive object:</p>
<pre>
def onNodeSelected(inter, kind, node0, node1, node2):
  if not(kind == v_sim.InteractivePick.SELECTED):
    return
  print "one node selected (%d)." % node0.number

interPick.connect("node-selection", onNodeSelected)
</pre>
<p>The signal node selection is used for single node selection but
also for distance selection and angle selection, thus the arguments
node0, node1 and node2.</p>

<h2>Interaction with Matplotlib</h2>
<p>Currently, Matplotlib is based on Gtk+2.0 while V_Sim uses
Gtk+3.0. This makes the Gtk Matplotlib widget unavailable to be
used. This will change when Matplotlib will be ported to
Gtk+3.0. Currently, one has to rely on the cairo backend of Matplotlib
and draw into a GtkDrawingArea.</p>
<pre>
from matplotlib.figure import Figure
from matplotlib.backends.backend_cairo import FigureCanvasCairo
from matplotlib.backends.backend_cairo import RendererCairo

def mplCurveDraw(widget, cr, f, renderer):
  renderer.gc.ctx = cr
  w = widget.get_allocated_width()
  h = widget.get_allocated_height()
  cr.translate(max(0, (w - fig_width * dpi) * 0.5),
               -max(0, (h - fig_height * dpi) * 0.5))
  renderer.set_width_height(w, h)
  f.draw(renderer)


f = Figure(figsize=(fig_width,fig_height), dpi = dpi)
f.set_canvas(FigureCanvasCairo(f))
renderer = RendererCairo(dpi = dpi)

area = Gtk.DrawingArea.new()
area.connect("draw", mplCurveDraw, f, renderer)
</pre>

    ]]></libre>
  </main>
</page>
