<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" indent="yes" encoding="utf-8"/>

  <xsl:include href="pagesWeb.xsl"/>

  <xsl:template match="/">
    <page key="dev-svnlog" lang="fr">
      <title>le coin des développeurs - liste des changements du SVN</title>
      <main>
        <section>Changements suivis par SVN dans l'archives</section>
        <libre>
          <xsl:text disable-output-escaping="yes"><![CDATA[<![CDATA[]]></xsl:text>
          <table cellpadding="0" cellspacing="0">
            <xsl:apply-templates select="bug">
              <!--              <xsl:sort select="etat/dateOuverture/@année" data-type="number" order="descending" />
              <xsl:sort select="etat/dateOuverture/@mois" data-type="number" order="descending" />
              <xsl:sort select="etat/dateOuverture/@jour" data-type="number" order="descending" />-->
            </xsl:apply-templates>
          </table>
          <xsl:text disable-output-escaping="yes">]]</xsl:text><xsl:text disable-output-escaping="yes"><![CDATA[>]]></xsl:text>
        </libre>
      </main>
    </page>
  </xsl:template>

</xsl:stylesheet>
