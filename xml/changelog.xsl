<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" indent="yes" encoding="utf-8" doctype-public="" />

  <xsl:include href="pagesWeb.xsl"/>

  <xsl:template match="/ChangeLog">
    <xsl:variable name="langPage">
      <xsl:value-of select="@lang" />
    </xsl:variable>
    <page key="dev-changelog" have-menu="yes">
      <xsl:attribute name="lang">
        <xsl:value-of select="$langPage" />
      </xsl:attribute>
      <title>
        <xsl:for-each select="document('webPages.xml')/webPages/developpement/changelog/titre/value">
          <xsl:if test="@lang = $langPage">
            <xsl:value-of select="text()" />
          </xsl:if>
        </xsl:for-each>
      </title>
      <main>
        <xsl:for-each select="milestone">
          <section>
            <xsl:attribute name="id">
              <xsl:value-of select="@version" />
            </xsl:attribute>
            <xsl:for-each select="document('webPages.xml')/webPages/developpement/changelog/version/value">
              <xsl:if test="@lang = $langPage">
                <xsl:value-of select="text()" /><xsl:text> </xsl:text>
              </xsl:if>
            </xsl:for-each>
            <xsl:value-of select="@version" />
            <xsl:if test="@date">
              <xsl:text>&#160;</xsl:text>
              <xsl:value-of select="@date" />
            </xsl:if>
          </section>
          <libre>
            <xsl:text disable-output-escaping="yes"><![CDATA[<![CDATA[]]></xsl:text>
            <p><xsl:value-of select="intro/text()" /></p>
            <xsl:for-each select="entry">
              <h2>
                <xsl:value-of select="@titre" />
                <xsl:if test="@jour != ''">
                  <xsl:for-each select="document('webPages.xml')/webPages/developpement/changelog/sortie/value">
                    <xsl:if test="@lang = $langPage">
                      <xsl:value-of select="text()" />
                    </xsl:if>
                  </xsl:for-each>
                  <xsl:call-template name="applyDate">
                    <xsl:with-param name="jour" select="@jour" />
                    <xsl:with-param name="mois" select="@mois" />
                    <xsl:with-param name="annee" select="@année" />
                    <xsl:with-param name="format" select="'%d/%m/%y'" />
                  </xsl:call-template>
                </xsl:if>
              </h2>
              
              <xsl:if test="li/@type">
                <ul>
                  <xsl:for-each select="li">
                    <xsl:sort select="@type"/>
                    <li>
                      <xsl:choose>
                        <xsl:when test="@type = 'capability'">
                          <span>
                            <xsl:attribute name="class">
                              <xsl:text>type_</xsl:text><xsl:value-of select="@type" />
                            </xsl:attribute>
                            <xsl:text>[capability]&#160;:: </xsl:text>
                          </span>
                        </xsl:when>
                        <xsl:when test="@type = 'fileFormat'">
                          <span>
                            <xsl:attribute name="class">
                              <xsl:text>type_</xsl:text><xsl:value-of select="@type" />
                            </xsl:attribute>
                            <xsl:text>[file format]&#160;:: </xsl:text>
                          </span>
                        </xsl:when>
                        <xsl:when test="@type = 'interface'">
                          <span>
                            <xsl:attribute name="class">
                              <xsl:text>type_</xsl:text><xsl:value-of select="@type" />
                            </xsl:attribute>
                            <xsl:text>[interface]&#160;:: </xsl:text>
                          </span>
                        </xsl:when>
                        <xsl:when test="@type = 'misc'">
                          <span>
                            <xsl:attribute name="class">
                              <xsl:text>type_</xsl:text><xsl:value-of select="@type" />
                            </xsl:attribute>
                            <xsl:text>[miscelaneous]&#160;:: </xsl:text>
                          </span>
                        </xsl:when>
                        <xsl:when test="@type = 'rendering'">
                          <span>
                            <xsl:attribute name="class">
                              <xsl:text>type_</xsl:text><xsl:value-of select="@type" />
                            </xsl:attribute>
                            <xsl:text>[rendering]&#160;:: </xsl:text>
                          </span>
                        </xsl:when>
                      </xsl:choose>
                      <xsl:value-of select="text()" />
                    </li>
                  </xsl:for-each>
                </ul>
              </xsl:if>
              <xsl:if test="not(li/@type)">
                <ul>
                  <xsl:for-each select="li">
                    <li><xsl:value-of select="text()" /></li>
                  </xsl:for-each>
                </ul>
              </xsl:if>
            </xsl:for-each>
            <xsl:text disable-output-escaping="yes">]]</xsl:text><xsl:text disable-output-escaping="yes"><![CDATA[>]]></xsl:text>
          </libre>
        </xsl:for-each>
      </main>
    </page>
  </xsl:template>

</xsl:stylesheet>

