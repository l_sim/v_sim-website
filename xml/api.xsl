<?xml version='1.0'?> <!--*- mode: xml -*-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:l="http://docbook.sourceforge.net/xmlns/l10n/1.0"
                exclude-result-prefixes="l"
                version="1.0">

  <xsl:include href="gtk-doc.xsl"/>
  <xsl:include href="pagesWeb.xsl"/>

  <xsl:template name="user.header.navigation">
    <xsl:call-template name="makeHeader">
      <xsl:with-param name="lang">en</xsl:with-param>
      <xsl:with-param name="key">api</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="user.header.content">
    <xsl:call-template name="makeMenu">
      <xsl:with-param name="lang">en</xsl:with-param>
      <xsl:with-param name="key">api</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="system.head.content">
    <xsl:call-template name="makeHead">
      <xsl:with-param name="lang">en</xsl:with-param>
      <xsl:with-param name="key">api</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

</xsl:stylesheet>
