<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml" indent="yes" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

  <xsl:include href="menu.xsl" />

  <xsl:template match="/page">
    <html>
      <xsl:attribute name="lang">
        <xsl:value-of select="@lang" />
      </xsl:attribute>
      <head>
        <title>V_Sim - <xsl:value-of select="title/text()" /></title>
        <xsl:call-template name="makeHead">
          <xsl:with-param name="lang" select="@lang" />
          <xsl:with-param name="key" select="@key" />
        </xsl:call-template>
      </head>
      <body>
        <xsl:call-template name="makeHeader">
          <xsl:with-param name="lang" select="@lang" />
          <xsl:with-param name="key" select="@key" />
        </xsl:call-template>

        <xsl:call-template name="makeMenu">
          <xsl:with-param name="lang" select="@lang" />
          <xsl:with-param name="key" select="@key" />
        </xsl:call-template>

        <!-- recherche d'un n~ud a parent dans menu.xml -->
        <xsl:variable name="parent">
          <xsl:variable name="key">
            <xsl:value-of select="@key" />
          </xsl:variable>
          <xsl:for-each select="document('menu.xml')/menu">
            <xsl:for-each select="key('pageId', $key)">
              <xsl:value-of select="../../@id" />
            </xsl:for-each>
          </xsl:for-each>
        </xsl:variable>
        <xsl:if test="$parent != ''">
          <xsl:call-template name="makeSubMenu">
            <xsl:with-param name="lang" select="@lang" />
            <xsl:with-param name="parent" select="$parent" />
            <xsl:with-param name="key" select="@key" />
          </xsl:call-template>
        </xsl:if>

        <xsl:apply-templates select="main">
          <xsl:with-param name="have-menu" select="@have-menu" />
        </xsl:apply-templates>

        <xsl:apply-templates select="document('webPages.xml')/webPages/footer">
          <xsl:with-param name="lang" select="@lang" />
          <xsl:with-param name="key" select="@key" />
        </xsl:apply-templates>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="makeHead">
    <xsl:param name="lang" />
    <xsl:param name="key" />

    <xsl:variable name="prefix"><xsl:if test="$key = 'api'">../</xsl:if></xsl:variable>
    <meta name="copyright" content="&#169; 2001-2011 CEA" />
    <link rel="shortcut icon" type="image/x-icon">
      <xsl:attribute name="href"><xsl:if test="$prefix and not($prefix = '')"><xsl:value-of select="$prefix" /></xsl:if>favicon.ico</xsl:attribute>
    </link>
    <link rel="stylesheet" title="Prefered" type="text/css">
      <xsl:attribute name="href"><xsl:if test="$prefix and not($prefix = '')"><xsl:value-of select="$prefix" /></xsl:if>styles/original.css</xsl:attribute>
    </link>
    <link rel="alternate stylesheet" title="CEA" type="text/css">
      <xsl:attribute name="href"><xsl:if test="$prefix and not($prefix = '')"><xsl:value-of select="$prefix" /></xsl:if>styles/cea.css</xsl:attribute>
    </link>
    <link rel="copyright" type="text/html" href="http://www.cecill.info" hreflang="fr" lang="fr" />
    <link rel="author" type="text/html" href="http://www.cea.fr" hreflang="fr" lang="fr" />
    <xsl:if test="not($key = 'api')">
      <xsl:call-template name="makeLink">
        <xsl:with-param name="lang" select="$lang" />
        <xsl:with-param name="key" select="$key" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="makeHeader">
    <xsl:param name="lang" />
    <xsl:param name="key" />

    <xsl:variable name="prefix"><xsl:if test="$key = 'api'">../</xsl:if></xsl:variable>
    <div class="header">
      <div class="visuLogo">
        <img alt="Logo V_Sim" width="102" height="80" style="margin:10px;">
          <xsl:attribute name="src"><xsl:if test="$prefix and not($prefix = '')"><xsl:value-of select="$prefix" /></xsl:if><xsl:text>images/logo_header.png</xsl:text></xsl:attribute>
        </img>
      </div>
      <div class="version">
        <xsl:apply-templates select="document('webPages.xml')/webPages/version">
          <xsl:with-param name="lang" select="$lang" />
        </xsl:apply-templates>
      </div>
      <!-- Teste l'existence d'autres langues -->
      <xsl:variable name="langueExiste">
        <xsl:variable name="localKey">
          <xsl:value-of select="$key" />
        </xsl:variable>
        <xsl:variable name="localLang">
          <xsl:value-of select="$lang" />
        </xsl:variable>
        <xsl:for-each select="document('webPages.xml')/webPages/liens/entry">
          <xsl:if test="@key = $localKey">
            <xsl:for-each select="value">
              <xsl:if test="not(@lang = $localLang) and @href">
                <xsl:text>Ok</xsl:text>
              </xsl:if>
            </xsl:for-each>
          </xsl:if>
        </xsl:for-each>
      </xsl:variable>
      <xsl:if test="not($langueExiste = '')">
        <div class="visuInfoLogo">
          <xsl:variable name="langPage">
            <xsl:value-of select="$lang" />
          </xsl:variable>
          <xsl:for-each select="document('webPages.xml')/webPages/langues/label">
            <xsl:if test="@lang = $langPage">
              <xsl:value-of select="text()" />
            </xsl:if>
          </xsl:for-each>
          <ul>
            <xsl:call-template name="lienLangues">
              <xsl:with-param name="lang"><xsl:value-of select="$lang" /></xsl:with-param>
              <xsl:with-param name="key"><xsl:value-of select="$key" /></xsl:with-param>
              <xsl:with-param name="enListe">true</xsl:with-param>
            </xsl:call-template>
          </ul>
        </div>
      </xsl:if>
    </div>
  </xsl:template>

  <xsl:template match="main">
    <xsl:param name="have-menu" />
    <xsl:variable name="lang">
      <xsl:value-of select="/page/@lang" />
    </xsl:variable>
    <div class="main">
      <xsl:if test="$have-menu = 'yes'">
        <div class="floatingmenu">
          <h3>
            <xsl:for-each select="document('webPages.xml')/webPages/menus/quickAccess/value">
              <xsl:if test="@lang = $lang"><xsl:value-of select="text()" /></xsl:if>
            </xsl:for-each>  
          </h3>
          <ul>
            <xsl:for-each select="section|subsection">
              <li>
                <xsl:attribute name="class">
                  <xsl:value-of select="name()" />
                </xsl:attribute>
                <a>
                  <xsl:attribute name="href">
                    <xsl:text>#</xsl:text><xsl:value-of select="@id" />
                  </xsl:attribute>
                  <xsl:if test="@id-label">
                    <xsl:value-of select="@id-label" />
                  </xsl:if>
                  <xsl:if test="not(@id-label)">
                    <xsl:value-of select="text()" />
                  </xsl:if>
                </a>
              </li>
            </xsl:for-each>
          </ul>
        </div>
      </xsl:if>
      <a name="top"></a>

      <xsl:apply-templates />
    </div>
  </xsl:template>

  <xsl:template match="section">
    <xsl:variable name="lang">
      <xsl:value-of select="/page/@lang" />
    </xsl:variable>
    <xsl:if test="@id">
      <a>
        <xsl:attribute name="name">
          <xsl:value-of select="@id" />
        </xsl:attribute>
      </a>
    </xsl:if>
    <h1>
      <xsl:value-of select="text()" />
      <xsl:if test="/page/@have-menu = 'yes'">
        <a href="#top" class="ontop">
          <xsl:text>&#8593;&#160;</xsl:text>
          <xsl:for-each select="document('webPages.xml')/webPages/news/onTop/value">
            <xsl:if test="@lang = $lang"><xsl:value-of select="text()" /></xsl:if>
          </xsl:for-each>
        </a>
      </xsl:if>
    </h1>
  </xsl:template>

  <xsl:template match="subsection">
    <xsl:variable name="lang">
      <xsl:value-of select="/page/@lang" />
    </xsl:variable>
    <xsl:if test="@id">
      <a>
        <xsl:attribute name="name">
          <xsl:value-of select="@id" />
        </xsl:attribute>
      </a>
    </xsl:if>
    <h2>
      <xsl:value-of select="text()" />
      <xsl:if test="/page/@have-menu = 'yes'">
        <a href="#top" class="ontop">
          <xsl:text>&#8593;&#160;</xsl:text>
          <xsl:for-each select="document('webPages.xml')/webPages/news/onTop/value">
            <xsl:if test="@lang = $lang"><xsl:value-of select="text()" /></xsl:if>
          </xsl:for-each>
        </a>
      </xsl:if>
    </h2>
  </xsl:template>

  <xsl:template match="news">
    <xsl:variable name="lang">
      <xsl:value-of select="/page/@lang" />
    </xsl:variable>
    <a>
      <xsl:if test="@id">
        <xsl:attribute name="name">
          <xsl:value-of select="@id" />
        </xsl:attribute>
      </xsl:if>
    </a>
    <div class="news">
      <h2>
        <xsl:apply-templates select="titre"/>
        <xsl:if test="@linkOnTop = 'yes'">
          <a href="#top" class="ontop">
            <xsl:text>&#8593;&#160;</xsl:text>
            <xsl:for-each select="document('webPages.xml')/webPages/news/onTop/value">
              <xsl:if test="@lang = $lang"><xsl:value-of select="text()" /></xsl:if>
            </xsl:for-each>
          </a>
        </xsl:if>
      </h2>
      <xsl:if test="@jour or @mois or @annee or @auteur or @infos">
        <div class="newsInfos">
          <xsl:if test="@jour and @mois and @annee and @auteur"> 
          <xsl:variable name="format">
            <xsl:for-each select="document('webPages.xml')/webPages/news/infos/value">
              <xsl:if test="@lang = $lang"><xsl:value-of select="text()" /></xsl:if>
            </xsl:for-each>
          </xsl:variable>
          <xsl:variable name="date">
            <xsl:call-template name="applyDate">
              <xsl:with-param name="jour" select="@jour" />
              <xsl:with-param name="mois" select="@mois" />
              <xsl:with-param name="annee" select="@annee" />
              <xsl:with-param name="format" select="$format" />
            </xsl:call-template>
          </xsl:variable>
          <xsl:call-template name="remplace">
            <xsl:with-param name="string" select="$date" />
            <xsl:with-param name="remplaceDe" select="'%s'" />
            <xsl:with-param name="remplaceVers" select="@auteur" />
          </xsl:call-template>
        </xsl:if>
        <xsl:if test="@infos">
          <xsl:value-of select="@infos" />
        </xsl:if>
      </div>
    </xsl:if>
    <xsl:apply-templates select="contenu"/>
    <hr class="newsending"></hr>
  </div>
</xsl:template>

  <xsl:template match="libre">
    <xsl:value-of select="text()" disable-output-escaping="yes"/>
  </xsl:template>

  <xsl:template match="a">
    <xsl:call-template name="lien">
      <xsl:with-param name="lang" select="/page/@lang" />
      <xsl:with-param name="langDest" select="@lang" />
      <xsl:with-param name="href" select="@href" />
      <xsl:with-param name="id" select="@id" />
      <xsl:with-param name="texte" select="text()" />
      <xsl:with-param name="param" select="@param" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="version">
    <xsl:param name="lang" />
    <xsl:variable name="format">
      <xsl:for-each select="value">
        <xsl:if test="@lang = $lang"><xsl:value-of select="text()" /></xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="date">
      <xsl:call-template name="applyDate">
        <xsl:with-param name="jour" select="@day" />
        <xsl:with-param name="mois" select="@month" />
        <xsl:with-param name="annee" select="@year" />
        <xsl:with-param name="format" select="$format" />
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="remplace">
      <xsl:with-param name="string" select="$date" />
      <xsl:with-param name="remplaceDe" select="'%s'" />
      <xsl:with-param name="remplaceVers" select="@id" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="date">
    <xsl:variable name="lang">
      <xsl:value-of select="/page/@lang" />
    </xsl:variable>
    <xsl:variable name="format">
      <xsl:for-each select="document('webPages.xml')/webPages/divers/date/value">
        <xsl:if test="@lang = $lang">
          <xsl:value-of select="text()" />    
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:call-template name="applyDate">
      <xsl:with-param name="jour" select="@jour" />
      <xsl:with-param name="mois" select="@mois" />
      <xsl:with-param name="annee" select="@annee" />
      <xsl:with-param name="format" select="$format" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="lienLangues">
    <xsl:param name="lang" />
    <xsl:param name="key" />
    <xsl:param name="enListe" />    

    <xsl:for-each select="document('webPages.xml')/webPages/liens/entry">
      <!-- link alternate -->
      <xsl:if test="@key = $key">
        <xsl:for-each select="value">
          <xsl:if test="not(@lang = $lang) and @href">
            <xsl:if test="$enListe = 'true'">
              <li>
                <xsl:apply-templates select="document('webPages.xml')/webPages/footer/language">
                  <xsl:with-param name="lang"><xsl:value-of select="$lang" /></xsl:with-param>
                  <xsl:with-param name="langDest"><xsl:value-of select="@lang" /></xsl:with-param>
                  <xsl:with-param name="link"><xsl:value-of select="@href" /></xsl:with-param>
                </xsl:apply-templates>
              </li>
            </xsl:if>
            <xsl:if test="$enListe = 'false'">
              <xsl:apply-templates select="document('webPages.xml')/webPages/footer/language">
                <xsl:with-param name="lang"><xsl:value-of select="$lang" /></xsl:with-param>
                <xsl:with-param name="langDest"><xsl:value-of select="@lang" /></xsl:with-param>
                <xsl:with-param name="link"><xsl:value-of select="@href" /></xsl:with-param>
              </xsl:apply-templates>
              <xsl:text> </xsl:text>
            </xsl:if>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="language">
    <xsl:param name="lang" />
    <xsl:param name="langDest" />
    <xsl:param name="link" />
    
    <xsl:for-each select="value">
      <xsl:if test="@lang = $langDest">
        <xsl:value-of select="text()" /><a><xsl:attribute name="href"><xsl:value-of select="$link" /></xsl:attribute><xsl:for-each select="document('webPages.xml')/webPages/langues/value"><xsl:if test="@lang=$langDest and @orig=$langDest"><xsl:value-of select="text()" /></xsl:if></xsl:for-each></a>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="footer">
    <xsl:param name="lang" />
    <xsl:param name="key" />
    <div class="footer">
      <div class="language">
        <xsl:call-template name="lienLangues">
          <xsl:with-param name="lang"><xsl:value-of select="$lang" /></xsl:with-param>
          <xsl:with-param name="key"><xsl:value-of select="$key" /></xsl:with-param>
          <xsl:with-param name="enListe">false</xsl:with-param>
        </xsl:call-template>
      </div>

      <xsl:variable name="phrase">
        <xsl:for-each select="webAdmin/value">
          <xsl:if test="@lang = $lang">
            <xsl:value-of select="text()" />
          </xsl:if>
        </xsl:for-each>
      </xsl:variable>
      <xsl:call-template name="remplace">
        <xsl:with-param name="string" select="$phrase" />
        <xsl:with-param name="remplaceDe" select="'%s'" />
        <xsl:with-param name="remplaceVers" select="webAdmin/@address" />
      </xsl:call-template>
      |
      <xsl:variable name="title">
        <xsl:for-each select="normesW3C/xhtml/value">
          <xsl:if test="@lang = $lang">
            <xsl:value-of select="text()" />
          </xsl:if>
        </xsl:for-each>
      </xsl:variable>
      <a href="http://validator.w3.org/check/referer"><xsl:attribute name="title"><xsl:value-of select="$title" /></xsl:attribute>XHTML1.0</a>
      <xsl:text> - </xsl:text>
      <xsl:variable name="title2">
        <xsl:for-each select="normesW3C/css/value">
          <xsl:if test="@lang = $lang">
            <xsl:value-of select="text()" />
          </xsl:if>
        </xsl:for-each>
      </xsl:variable>
      <a href="http://jigsaw.w3.org/css-validator/check/referer"><xsl:attribute name="title"><xsl:value-of select="$title2" /></xsl:attribute>CSS2</a>
      |
      <xsl:variable name="format">
        <xsl:for-each select="date/value">
          <xsl:if test="@lang = $lang"><xsl:value-of select="text()" /></xsl:if>
        </xsl:for-each>
      </xsl:variable>
      <xsl:call-template name="applyDate">
        <xsl:with-param name="jour" select="document('date.xml')/date/@jour" />
        <xsl:with-param name="mois" select="document('date.xml')/date/@mois" />
        <xsl:with-param name="annee" select="document('date.xml')/date/@annee" />
        <xsl:with-param name="format" select="$format" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template name="applyDate">
    <xsl:param name="jour" />
    <xsl:param name="mois" />
    <xsl:param name="annee" />
    <xsl:param name="format" />
    <xsl:variable name="output1">
      <xsl:call-template name="remplace">
        <xsl:with-param name="string" select="$format" />
        <xsl:with-param name="remplaceDe" select="'%d'" />
        <xsl:with-param name="remplaceVers" select="format-number($jour,'00')" />
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="output2">
      <xsl:call-template name="remplace">
        <xsl:with-param name="string" select="$output1" />
        <xsl:with-param name="remplaceDe" select="'%y'" />
        <xsl:with-param name="remplaceVers" select="$annee" />
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="remplace">
      <xsl:with-param name="string" select="$output2" />
      <xsl:with-param name="remplaceDe" select="'%m'" />
      <xsl:with-param name="remplaceVers" select="format-number($mois,'00')" />
    </xsl:call-template>
  </xsl:template>

  <!-- Fonction de remplacement dans une chaîne -->
  <xsl:template name="remplace">
    <xsl:param name="string" />
    <xsl:param name="remplaceDe" />
    <xsl:param name="remplaceVers" />
    <xsl:if test="contains($string, $remplaceDe)">
      <xsl:value-of select="substring-before($string, $remplaceDe)" />
      <xsl:value-of select="$remplaceVers"/>
      <xsl:call-template name="remplace">
        <xsl:with-param name="string">
          <xsl:value-of select="substring-after($string, $remplaceDe)" />
        </xsl:with-param>
        <xsl:with-param name="remplaceDe">
          <xsl:value-of select="$remplaceDe" />
        </xsl:with-param>
        <xsl:with-param name="remplaceVers">
          <xsl:value-of select="$remplaceVers" />
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test="not(contains($string, $remplaceDe))">
      <xsl:value-of select="$string" />
    </xsl:if>
  </xsl:template>

  <xsl:template match="import">
    <xsl:apply-templates select="document(@file)" />    
  </xsl:template>

  <xsl:template match="configFile">
    <table cellspacing="0" cellpadding="0">
      <tr><th>Keyword</th><th>Meaning</th><th>New from</th></tr>
      <xsl:for-each select="entry">
        <xsl:sort select="@name" />
        <tr>
          <xsl:if test="obsolete">
            <xsl:attribute name="class">
              <xsl:text>obsolete</xsl:text>
            </xsl:attribute>
          </xsl:if>
          <td class="tdname">
            <a><xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute></a>
            <code><xsl:value-of select="@name" /></code>
            <xsl:if test="obsolete"><xsl:text> </xsl:text><em>(obsolete)</em></xsl:if>
          </td>
          <td><xsl:value-of select="description/text()" /></td>
          <td class="tdversion">
            <xsl:if test="not(@version = 3.0)">version <xsl:value-of select="@version" /></xsl:if>
            <xsl:if test="not(@version = 3.0) and obsolete"> | </xsl:if>
            <xsl:if test="obsolete">replaced by <code><a><xsl:attribute name="href">#<xsl:value-of select="obsolete/@replacedBy" /></xsl:attribute><xsl:value-of select="obsolete/@replacedBy" /></a></code></xsl:if>
          </td>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>

  <xsl:template match="commandLine">
    <table cellspacing="0" cellpadding="0">
      <tr><th>Keyword</th><th>Meaning</th><th>New from</th></tr>
      <xsl:for-each select="option">
        <tr>
          <td class="tdname">
            <code>
              <xsl:if test="@short">
                <xsl:text>-</xsl:text><xsl:value-of select="@short" /><xsl:text>, </xsl:text>
              </xsl:if>
              <xsl:text>--</xsl:text><xsl:value-of select="@name" />
              <xsl:if test="description/@arg">
                <xsl:text> </xsl:text><xsl:value-of select="description/@arg" />
              </xsl:if>
            </code>
            <xsl:if test="description/@default">
              <br />
              <xsl:text>(Default value: </xsl:text><xsl:value-of select="description/@default" /><xsl:text>)</xsl:text>
            </xsl:if>
          </td>
          <td><xsl:value-of select="description/text()" /></td>
          <td class="tdversion">
            <xsl:text>version </xsl:text><xsl:value-of select="@version" /><xsl:text>.0</xsl:text>
          </td>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>

</xsl:stylesheet>
