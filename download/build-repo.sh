#!/bin/bash

topdir=$PWD
releases="hardy lucid natty"
categories="main dev"
architectures="i386 amd64"

for release in $releases; do
    cd $topdir
    chmod -R go+rX *
    for category in $categories; do
        for architecture in $architectures; do
            mkdir -p dists/$release/$category/binary-$architecture
            dpkg-scanpackages -a $architecture pool/$release/$category /dev/null \
                2>/dev/null > \
                dists/$release/$category/binary-$architecture/Packages
            gzip -c dists/$release/$category/binary-$architecture/Packages > \
                dists/$release/$category/binary-$architecture/Packages.gz

            mkdir -p dists/$release/$category/source
            dpkg-scansources pool/$release/$category /dev/null \
                2>/dev/null > \
                dists/$release/$category/source/Sources
            gzip -c dists/$release/$category/source/Sources > \
                dists/$release/$category/source/Sources.gz

        done
    done
    cd dists/$release
    rm Release Release.gpg
    apt-ftparchive release . \
        -o APT::FTPArchive::Release::Origin="CEA L_Sim (France)" \
        -o APT::FTPArchive::Release::Codename=$release \
        > /tmp/Release
    mv /tmp/Release .
    gpg -abs -o Release.gpg Release
done

cd $topdir
chmod -R o+rX dists pool
