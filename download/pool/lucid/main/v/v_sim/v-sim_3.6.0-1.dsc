-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: v-sim
Binary: v-sim, v-sim-common, v-sim-doc, v-sim-plugins
Architecture: any
Version: 3.6.0-1
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Sylvestre Ledru <sylvestre@debian.org>, Damien Caliste <damien.caliste@cea.fr>
Homepage: http://inac.cea.fr/L_Sim/V_Sim/index.en.html
Standards-Version: 3.9.2.0
Vcs-Browser: http://svn.debian.org/viewsvn/debian-science/packages/v-sim/
Vcs-Svn: svn://svn.debian.org/svn/debian-science/packages/v-sim/
Build-Depends: debhelper (>= 5), cdbs, autotools-dev, mesa-common-dev, libglu1-mesa-dev, pkg-config, libglib2.0-dev, libgtk2.0-dev, libnetcdf-dev, libopenbabel-dev (>= 2.2.0), gtk-doc-tools, gfortran
Checksums-Sha1: 
 2fa79fcf3af3170e7d54953df9b5842b055e0bf9 3529708 v-sim_3.6.0.orig.tar.gz
 e5c01d8156be9b73e39bdd098d1f00b527a5a12a 12624 v-sim_3.6.0-1.debian.tar.gz
Checksums-Sha256: 
 0e6b4a496a3338725a89c48732c7d17375f5bd281fb8d385d408c1533cad3231 3529708 v-sim_3.6.0.orig.tar.gz
 0d73b01dfe1d640b3c431d7c374973bfb05f647c642a6e801852e90ed2c98f0e 12624 v-sim_3.6.0-1.debian.tar.gz
Files: 
 57882aeff4a50c2659b4392c533be302 3529708 v-sim_3.6.0.orig.tar.gz
 17a066c067fa27039d511bde411ee0dc 12624 v-sim_3.6.0-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAk3s1hAACgkQbcvrAViCxZABCwCeIgoDheJHAQ6gerEtovFFpR/e
fn8An1UuiRnpcPOeQApVxrvmlXHpfA6b
=aQ/5
-----END PGP SIGNATURE-----
