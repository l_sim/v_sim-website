Format: 3.0 (quilt)
Source: v-sim
Binary: v-sim, v-sim-common, v-sim-doc, v-sim-plugins
Architecture: any
Version: 3.6.99.0-1
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Sylvestre Ledru <sylvestre@debian.org>, Damien Caliste <damien.caliste@cea.fr>
Homepage: http://inac.cea.fr/L_Sim/V_Sim/index.en.html
Standards-Version: 3.9.2.0
Vcs-Browser: http://svn.debian.org/viewsvn/debian-science/packages/v-sim/
Vcs-Svn: svn://svn.debian.org/svn/debian-science/packages/v-sim/
Build-Depends: debhelper (>= 5), cdbs, autotools-dev, mesa-common-dev, libglu1-mesa-dev, pkg-config, libglib2.0-dev, libgtk2.0-dev, libnetcdf-dev, libopenbabel-dev (>= 2.2.0), gtk-doc-tools, gfortran
Checksums-Sha1: 
 f97c94948da48d47c98eeab353381f7a8aebad48 3607985 v-sim_3.6.99.0.orig.tar.gz
 b037dab9dcff4212cfe1bf62e9fd1ad7e8a74bf8 12839 v-sim_3.6.99.0-1.debian.tar.gz
Checksums-Sha256: 
 1dd96b0c371f7f7abee7198d51bf4624901342da8889e567170d4cbee06c49a5 3607985 v-sim_3.6.99.0.orig.tar.gz
 b03505c30e96f97bd66f5ec6ac4a505cce6ab01b91a9aaa9bfc8aaca647ae049 12839 v-sim_3.6.99.0-1.debian.tar.gz
Files: 
 60040eb5a21c770192f5a3ab7eaba7df 3607985 v-sim_3.6.99.0.orig.tar.gz
 841497da378c7272d613ef70f32c9df0 12839 v-sim_3.6.99.0-1.debian.tar.gz
