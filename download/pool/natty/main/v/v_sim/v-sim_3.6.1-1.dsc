Format: 3.0 (quilt)
Source: v-sim
Binary: v-sim, v-sim-common, v-sim-doc, v-sim-plugins
Architecture: any
Version: 3.6.1-1
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Sylvestre Ledru <sylvestre@debian.org>, Damien Caliste <damien.caliste@cea.fr>
Homepage: http://inac.cea.fr/L_Sim/V_Sim/index.en.html
Standards-Version: 3.9.2.0
Vcs-Browser: http://svn.debian.org/viewsvn/debian-science/packages/v-sim/
Vcs-Svn: svn://svn.debian.org/svn/debian-science/packages/v-sim/
Build-Depends: debhelper (>= 5), cdbs, autotools-dev, mesa-common-dev, libglu1-mesa-dev, pkg-config, libglib2.0-dev, libgtk2.0-dev, libnetcdf-dev, libopenbabel-dev (>= 2.2.0), gtk-doc-tools, gfortran
Checksums-Sha1: 
 2d1050e045995c5632c7570055b717d63b02a8e3 3600968 v-sim_3.6.1.orig.tar.gz
 fe911d36b027a686ad4c02fccc36320a90b83189 13899 v-sim_3.6.1-1.debian.tar.gz
Checksums-Sha256: 
 832c3375c960a658459cb494ee131adc5a1112a5461bb55320d33c191103ec5a 3600968 v-sim_3.6.1.orig.tar.gz
 f5739a198a134bf4c145a50b061321a3a8069af33a9a346bc58fce47f17d0570 13899 v-sim_3.6.1-1.debian.tar.gz
Files: 
 91aaf6ab53a60229a3a3c5e6f3a36857 3600968 v-sim_3.6.1.orig.tar.gz
 3e561ef34f41f6295243b4770600594a 13899 v-sim_3.6.1-1.debian.tar.gz
