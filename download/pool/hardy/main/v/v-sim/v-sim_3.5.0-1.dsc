-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: v-sim
Binary: v-sim, v-sim-common, v-sim-doc, v-sim-plugins
Architecture: any
Version: 3.5.0-1
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Sylvestre Ledru <sylvestre.ledru@inria.fr>, Torsten Werner <twerner@debian.org>, Damien Caliste <damien.caliste@cea.fr>
Dm-Upload-Allowed: yes
Homepage: http://inac.cea.fr/L_Sim/V_Sim/index.en.html
Standards-Version: 3.8.0
Vcs-Browser: http://svn.debian.org/viewsvn/debian-science/packages/v-sim/
Vcs-Svn: svn://svn.debian.org/svn/debian-science/packages/v-sim/
Build-Depends: autotools-dev, cdbs, debhelper (>= 5), gtk-doc-tools, libglib2.0-dev, libglu1-mesa-dev, libgtk2.0-dev, libnetcdf-dev, libopenbabel-dev (>= 2.0.0), mesa-common-dev, pkg-config
Files: 
 e63739299c6a2c229ae5b8983b730f96 3261584 v-sim_3.5.0.orig.tar.gz
 55be76bd3ca9e08e46bbf92fa9dffc57 12190 v-sim_3.5.0-1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.6 (GNU/Linux)

iD8DBQFK8Ec2bcvrAViCxZARAjPqAJ42qwa9NBHD0LQxQGRr4JoP3GB66gCfS62O
uIk6fJ3AuWEaBMXwMjBCbd0=
=I5bq
-----END PGP SIGNATURE-----
