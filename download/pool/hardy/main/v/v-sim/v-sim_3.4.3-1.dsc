-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: v-sim
Binary: v-sim, v-sim-common, v-sim-doc, v-sim-plugins
Architecture: any
Version: 3.4.3-1
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Sylvestre Ledru <sylvestre.ledru@inria.fr>, Torsten Werner <twerner@debian.org>
Dm-Upload-Allowed: yes
Homepage: http://inac.cea.fr/L_Sim/V_Sim/index.en.html
Standards-Version: 3.8.0
Vcs-Browser: http://svn.debian.org/viewsvn/debian-science/packages/v-sim/
Vcs-Svn: svn://svn.debian.org/svn/debian-science/packages/v-sim/
Build-Depends: autotools-dev, cdbs, debhelper (>= 5), gtk-doc-tools, libglib2.0-dev, libglu1-mesa-dev, libgtk2.0-dev, libnetcdf-dev, libopenbabel-dev (>= 2.1.0), mesa-common-dev, pkg-config
Files: 
 3f444f4af863106015c32b0ca9b61941 3132474 v-sim_3.4.3.orig.tar.gz
 ec0662abd10d7030a4ba8fb9a23c216a 22552 v-sim_3.4.3-1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.6 (GNU/Linux)

iD8DBQFJmWsbbcvrAViCxZARAvHSAJ9KdS5RpMgi1vVEY0weaOhUUef+oACbB2Sr
si3nmM1jIxAy1ADCjs1WoXQ=
=VlMo
-----END PGP SIGNATURE-----
