-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: v-sim
Binary: v-sim, v-sim-plugins
Architecture: any
Version: 3.4.0-2
Maintainer: Damien Caliste <damien.caliste@cea.fr>
Standards-Version: 3.6.1
Build-Depends: autotools-dev, debhelper (>= 4.0.0), libglu1-mesa-dev, libgtk2.0-dev (>= 2.4.1-1), libopenbabel-dev, mesa-common-dev, netcdfg-dev
Files: 
 4f377804eddc0046e94a867726079e7a 3121712 v-sim_3.4.0.orig.tar.gz
 2f06c855dc203a38bcc45b4733870bd7 308091 v-sim_3.4.0-2.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.6 (GNU/Linux)

iD8DBQFIQ/ETbcvrAViCxZARAjG7AKCYtSKHOJ4jTLVt0FrCvvBmz/tcZQCbB8d9
hDMCiVkU4iGbqyioCT7EBwo=
=4Rv8
-----END PGP SIGNATURE-----
