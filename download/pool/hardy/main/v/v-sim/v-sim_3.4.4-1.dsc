-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: v-sim
Binary: v-sim, v-sim-common, v-sim-doc, v-sim-plugins
Architecture: any
Version: 3.4.4-1
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Sylvestre Ledru <sylvestre.ledru@inria.fr>, Torsten Werner <twerner@debian.org>
Dm-Upload-Allowed: yes
Homepage: http://inac.cea.fr/L_Sim/V_Sim/index.en.html
Standards-Version: 3.8.0
Vcs-Browser: http://svn.debian.org/viewsvn/debian-science/packages/v-sim/
Vcs-Svn: svn://svn.debian.org/svn/debian-science/packages/v-sim/
Build-Depends: autotools-dev, cdbs, debhelper (>= 5), gtk-doc-tools, libglib2.0-dev, libglu1-mesa-dev, libgtk2.0-dev, libnetcdf-dev, libopenbabel-dev (>= 2.0.0), mesa-common-dev, pkg-config
Files: 
 70733aad66b9ad5890b5df487b1d9cc8 3131858 v-sim_3.4.4.orig.tar.gz
 5213cf2255cc8de7add32a0b1158ee51 11272 v-sim_3.4.4-1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.6 (GNU/Linux)

iD8DBQFKJodCbcvrAViCxZARAqa6AJ95Er7UTmOfRytCxXt1A7OFm5fOvQCfZ6HV
P6G4Hdf3f9nVRL7eLl/WPAU=
=+6Yr
-----END PGP SIGNATURE-----
