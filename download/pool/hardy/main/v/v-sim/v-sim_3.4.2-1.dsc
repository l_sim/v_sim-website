Format: 1.0
Source: v-sim
Binary: v-sim, v-sim-common, v-sim-doc, v-sim-plugins
Architecture: any
Version: 3.4.2-1
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Sylvestre Ledru <sylvestre.ledru@inria.fr>, Torsten Werner <twerner@debian.org>
Dm-Upload-Allowed: yes
Homepage: http://inac.cea.fr/L_Sim/V_Sim/index.en.html
Standards-Version: 3.8.0
Vcs-Browser: http://svn.debian.org/viewsvn/debian-science/packages/v-sim/
Vcs-Svn: svn://svn.debian.org/svn/debian-science/packages/v-sim/
Build-Depends: autotools-dev, cdbs, debhelper (>= 5), gtk-doc-tools, libglib2.0-dev, libglu1-mesa-dev, libgtk2.0-dev, libnetcdf-dev, libopenbabel-dev (>= 2.1.0), mesa-common-dev, pkg-config
Files: 
 2e28ec5ab0fb4550eee8aa5d4c54ecd6 3157039 v-sim_3.4.2.orig.tar.gz
 9a0ebaf7fb51ef6cc73cc10280584e3d 546 v-sim_3.4.2-1.diff.gz
