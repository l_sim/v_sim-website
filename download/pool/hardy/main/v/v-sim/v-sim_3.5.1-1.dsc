-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: v-sim
Binary: v-sim, v-sim-common, v-sim-doc, v-sim-plugins
Architecture: any
Version: 3.5.1-1
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Sylvestre Ledru <sylvestre.ledru@inria.fr>, Torsten Werner <twerner@debian.org>, Damien Caliste <damien.caliste@cea.fr>
Dm-Upload-Allowed: yes
Homepage: http://inac.cea.fr/L_Sim/V_Sim/index.en.html
Standards-Version: 3.8.0
Vcs-Browser: http://svn.debian.org/viewsvn/debian-science/packages/v-sim/
Vcs-Svn: svn://svn.debian.org/svn/debian-science/packages/v-sim/
Build-Depends: autotools-dev, cdbs, debhelper (>= 5), gtk-doc-tools, libglib2.0-dev, libglu1-mesa-dev, libgtk2.0-dev, libnetcdf-dev, libopenbabel-dev (>= 2.0.0), mesa-common-dev, pkg-config
Files: 
 552a33c55bbfc0f19db35d0a35a138af 3265596 v-sim_3.5.1.orig.tar.gz
 18c48f4f53c4997a84226d499d4866fa 12251 v-sim_3.5.1-1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.6 (GNU/Linux)

iD8DBQFLi4eibcvrAViCxZARAqM8AJ9fxZbsEvRcZjWuGR9hNc7FHk7s6wCgjujf
AP6bdb9NC8Z3KhBUp67Z1js=
=0vm1
-----END PGP SIGNATURE-----
