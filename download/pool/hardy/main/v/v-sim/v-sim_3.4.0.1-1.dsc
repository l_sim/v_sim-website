-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: v-sim
Binary: v-sim, v-sim-plugins
Architecture: any
Version: 3.4.0.1-1
Maintainer: Damien Caliste <damien.caliste@cea.fr>
Standards-Version: 3.6.1
Build-Depends: autotools-dev, debhelper (>= 4.0.0), libglu1-mesa-dev, libgtk2.0-dev (>= 2.4.1-1), libopenbabel-dev, mesa-common-dev, netcdfg-dev
Files: 
 a56b9777ede833d7898d4a8640da4d61 3121095 v-sim_3.4.0.1.orig.tar.gz
 d92002b804ede3cb9591ba935512cd26 20 v-sim_3.4.0.1-1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.6 (GNU/Linux)

iD8DBQFIR6/YbcvrAViCxZARAj9QAKCKPqsNuapc9mkEVvd977AoFFSYfQCgnC08
9NZOqPysYggcn3ovEIzvzOw=
=PbvU
-----END PGP SIGNATURE-----
