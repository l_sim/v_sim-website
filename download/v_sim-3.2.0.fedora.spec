%define name    v_sim
%define longName V_Sim
%define version 3.2.0
%define release 3

Name:           %{name} 
Summary:        3D visualization package
Version:        %{version} 
Release:        %{release} 
#Source0:        http://www-drfmc.cea.fr/sp2m/L_Sim/V_Sim/download/%{name}-%{version}.tar.bz2
Source0:        %{name}-%{version}.tar.bz2  
Source1:        %{name}.16.png
Source2:        %{name}.32.png
Source3:        %{name}.48.png
URL:            http://www-drfmc.cea.fr/sp2m/L_Sim/V_Sim/

Group:          Applications/Engineering
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
License:        CeCILL

Packager:       Damien Caliste <dcaliste@free.fr>

%description
 This package contains a visualization program that renders
 through OpenGL atomic systems such as molecules or solid
 defects.
 
 Some functionalities come with the program, such as 
 colorizing atoms, exporting view to images (TIF, Postscript,
 PDF...), rotating the camera, rendering a set of files,
 get informations on atom positions.
 
 Input files are both binary or plain text files. In particular
 XYZ common format is recognized.


%prep 
%setup

%build 
%configure --enable-gtk-doc
%make

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=%buildroot install
%find_lang %{name}
# Install menu entry
%{__cat} > %{name}.desktop << EOF
[Desktop Entry]
Name=%{longName}
Comment=Visualize 3D atomic systems
Exec=%{_bindir}/%{name}
Icon=%{name}.png
Terminal=false
Type=Application
Categories=Application;Edutainment;Science;Physics;
Encoding=UTF-8
EOF
%{__mkdir_p} %{buildroot}%{_datadir}/applications
desktop-file-install \
    --vendor %{name} \
    --dir %{buildroot}%{_datadir}/applications  \
    %{name}.desktop
install -m644 %{SOURCE1} -D $RPM_BUILD_ROOT%{_miconsdir}/%{name}.png
install -m644 %{SOURCE2} -D $RPM_BUILD_ROOT%{_iconsdir}/%{name}.png
install -m644 %{SOURCE3} -D $RPM_BUILD_ROOT%{_liconsdir}/%{name}.png


%clean 
rm -rf $RPM_BUILD_ROOT

%post
# Updating icons
update-desktop-database &> /dev/null ||:

%postun
# Updating icons
update-desktop-database &> /dev/null ||:

%files -f %{name}.lang
%defattr(-,root,root,0755)

#Doc files
%dir %_datadir/doc/%{name}/APIreference/
%doc %_datadir/doc/%{name}/APIreference/*.html
%doc %_datadir/doc/%{name}/APIreference/*.png
%doc %_datadir/doc/%{name}/APIreference/*.css
%doc %_datadir/doc/%{name}/APIreference/index.sgml
%doc %_datadir/doc/%{name}/APIreference/v_sim.devhelp
%doc %_datadir/doc/%{name}/licence.*.txt
%doc %_datadir/doc/%{name}/*_help
%doc %_datadir/doc/%{name}/authors
%doc %_datadir/doc/%{name}/readme
%dir %_datadir/doc/%{name}/examples/
%doc %_datadir/doc/%{name}/examples/*

#Bin files
%_bindir/%{name}

#data files
%dir %_datadir/%{name}
%_datadir/%{name}/%{name}.*

#Pixmap files
%dir %_datadir/%{name}/pixmaps
%_datadir/%{name}/pixmaps/*

#Icons files
%_datadir/pixmaps/%{name}.xpm
%{_miconsdir}/%{name}.png
%{_iconsdir}/%{name}.png
%{_liconsdir}/%{name}.png

#Menu files
%{_datadir}/applications/%{name}.desktop



%changelog
* Tue May 30 2006 Damien Caliste <dcaliste@free.fr> 3.2.0-3
- Correction of a memory corruption bug in surfaces.c..

* Fri May 26 2006 Damien Caliste <dcaliste@free.fr> 3.2.0-2
- Move menu config to Fedora standards.
- Update spec file to remove French characters.

* Fri May 26 2006 Damien Caliste <dcaliste@free.fr> 3.2.0-1
- Update to upstream 3.2.0 version.
- This is a new features version, including enhanced interactiveness,
  modified colorisation tool, new planes features (saving/loading...).

* Fri Dec 09 2005 Damien Caliste <dcaliste@free.fr> 3.1.2-1
- Update to upstream 3.1.2 version (correcting resources bugs).

* Mon Oct 10 2005 Damien Caliste <dcaliste@free.fr> 3.1.1-1
- Update to upstream 3.1.1 version.

* Tue Sep 27 2005 Damien Caliste <caliste@drfmc.ceng.cea.fr> 3.1.0-3
- Adding a Packager tag.
- Correction des adresses mails.
- Correction de l'association des fichiers textes to V_Sim.

* Tue Sep 27 2005 Damien Caliste <caliste@drfmc.ceng.cea.fr> 3.1.0-2
- Ajout des fichiers d'internationalisation.
- Ajout des fichiers d'exemple.
- Adding a menu entry.

* Tue Sep 27 2005 Damien Caliste <caliste@drfmc.ceng.cea.fr> 3.1.0-1
- Initial build.

