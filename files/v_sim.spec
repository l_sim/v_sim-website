%define name    v_sim
%define version 3.1.0 
%define release 3

Name:           %{name} 
Summary:        3D visualization package
Version:        %{version} 
Release:        %{release} 
Source0:        http://www-drfmc.cea.fr/sp2m/L_Sim/V_Sim/download/%{name}-%{version}.tar.bz2 
Source1:        %{name}.16.png
Source2:        %{name}.32.png
Source3:        %{name}.48.png
URL:            http://www-drfmc.cea.fr/sp2m/L_Sim/V_Sim/

Group:          Sciences/Physics
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
License:        CeCILL

Packager:       Damien Caliste <caliste@drfmc.ceng.cea.fr>

%description
 This package contains a visualization program that renders
 through OpenGL atomic systems such as molecules or solid
 defects.
 
 Some functionalities come with the program, such as 
 colorizing atoms, exporting view to images (TIF, Postscript,
 PDF...), rotating the camera, rendering a set of files,
 get informations on atom positions.
 
 Input files are both binary or plain text files. In particular
 XYZ common format is recognized.


%prep 
%setup

%build 
%configure --enable-gtk-doc
%make

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=%buildroot install
%find_lang %{name}
#Create menu file
install -m755 -d $RPM_BUILD_ROOT%{_menudir}
cat > $RPM_BUILD_ROOT%{_menudir}/%{name} << EOF
?package(%{name}):\
 command="/usr/bin/%{name}"\
 icon="%{name}.png"\
 needs="X11"\
 section="More Applications/Sciences/Physics"\
 title="V_Sim"\
 longtitle="Visualiaze 3D atomic systems"\
 mimetypes=""\
 accept_url="true" \
 multiple_files="true" 
EOF
install -m644 %{SOURCE1} -D $RPM_BUILD_ROOT%{_miconsdir}/%{name}.png
install -m644 %{SOURCE2} -D $RPM_BUILD_ROOT%{_iconsdir}/%{name}.png
install -m644 %{SOURCE3} -D $RPM_BUILD_ROOT%{_liconsdir}/%{name}.png


%clean 
rm -rf $RPM_BUILD_ROOT

%post
%{update_menus}

%postun
%{clean_menus}

%files -f %{name}.lang
%defattr(-,root,root,0755)

#Doc files
%dir %_datadir/doc/%{name}/APIreference/
%doc %_datadir/doc/%{name}/APIreference/*.html
%doc %_datadir/doc/%{name}/APIreference/*.png
%doc %_datadir/doc/%{name}/APIreference/*.css
%doc %_datadir/doc/%{name}/APIreference/index.sgml
%doc %_datadir/doc/%{name}/APIreference/v_sim.devhelp
%doc %_datadir/doc/%{name}/licence.*.txt
%doc %_datadir/doc/%{name}/*_help
%doc %_datadir/doc/%{name}/authors
%doc %_datadir/doc/%{name}/readme
%dir %_datadir/doc/%{name}/examples/
%doc %_datadir/doc/%{name}/examples/*

#Bin files
%_bindir/%{name}

#data files
%dir %_datadir/%{name}
%_datadir/%{name}/%{name}.*

#Pixmap files
%dir %_datadir/%{name}/pixmaps
%_datadir/%{name}/pixmaps/*

#Icons files
%_datadir/pixmaps/%{name}.xpm
%{_miconsdir}/%{name}.png
%{_iconsdir}/%{name}.png
%{_liconsdir}/%{name}.png

#Menu files
%{_libdir}/menu/%{name}



%changelog
* Tue Sep 27 2005 Damien Caliste <caliste@drfmc.ceng.cea.fr> 3.1.0-3
- Ajout d'un tag Packager.
- Correction des adresses mails.
- Correction de l'association des fichiers textes � V_Sim.

* Tue Sep 27 2005 Damien Caliste <caliste@drfmc.ceng.cea.fr> 3.1.0-2
- Ajout des fichiers d'internationalisation.
- Ajout des fichiers d'exemple.
- Ajout d'une entr�e dans le menu.

* Tue Sep 27 2005 Damien Caliste <caliste@drfmc.ceng.cea.fr> 3.1.0-1
- Initial build.

