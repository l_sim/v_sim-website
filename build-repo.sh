#!/bin/bash

topdir="/local/caliste/v_sim-web/download"
releases="precise sid"
categories="main"
architectures="i386 amd64"

for release in $releases; do
    echo "Working for release: "$release
    cd $topdir
    for category in $categories; do
	echo " | in category: "$category
        for architecture in $architectures; do
	    echo " | and architecture: "$architecture
            mkdir -p dists/$release/$category/binary-$architecture
	    echo -n "Scanning pool 'pool/$release/$category' for packages..."
            dpkg-scanpackages -a $architecture pool/$release/$category /dev/null \
                2>/dev/null > \
                dists/$release/$category/binary-$architecture/Packages
            gzip -c dists/$release/$category/binary-$architecture/Packages > \
                dists/$release/$category/binary-$architecture/Packages.gz

            mkdir -p dists/$release/$category/source
            dpkg-scansources pool/$release/$category /dev/null \
                2>/dev/null > \
                dists/$release/$category/source/Sources
            gzip -c dists/$release/$category/source/Sources > \
                dists/$release/$category/source/Sources.gz
	    echo "done"
        done
    done
    cd dists/$release
    rm Release Release.gpg
    apt-ftparchive release . \
        -o APT::FTPArchive::Release::Origin="CEA L_Sim (France)" \
        -o APT::FTPArchive::Release::Codename=$release \
        > /tmp/Release
    mv /tmp/Release .
    gpg -abs -o Release.gpg Release
done

cd $topdir
chmod -R o+rX dists pool
